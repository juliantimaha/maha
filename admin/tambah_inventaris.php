<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inventaris</title>

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="css/local.css" />

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

    <!-- you need to include the shieldui css and js assets in order for the charts to work -->
    <link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/shieldui-all.min.css" />
    <link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css" />
    <link id="gridcss" rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/dark-bootstrap/all.min.css" />

    <script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="http://www.prepbootstrap.com/Content/js/gridData.js"></script>
</head>
<body>
    <div id="wrapper">
          <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Inventaris</a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
               <ul id="active" class="nav navbar-nav side-nav">
                    <li><a href="index.php"><i class="fa fa-list"></i> Inventarisir</a></li>
                 <li><a href="proses_inventaris.php" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-info"><br></i> Proses Inventaris</a>
					<ul class="dropdown-menu">
                            <li><a href="peminjaman.php"><i class="fa fa-share"></i> Peminjaman</a></li>
                            <li><a href="pengembalian.php"><i class="fa fa-reply"></i> Pengembalian</a></li>
                        </ul>
                    <li><a href="master_data.php" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i> Master Data</a>
					<ul class="dropdown-menu">
                            <li><a href="Jenis.php"><i class="fa fa-caret-down"></i> Jenis</a></li>
                            <li><a href="Level.php"><i class="fa fa-caret-down"></i> level</a></li>
							<li><a href="Ruang.php"><i class="fa fa-caret-down"></i> Ruang</a></li>
                            <li><a href="Pegawai.php"><i class="fa fa-caret-down"></i> Pegawai</a></li>
                        </ul>
                </ul>
                <ul class="nav navbar-nav navbar-right navbar-user">
                    <li class="dropdown messages-dropdown">
           
                    <li class="dropdown user-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>Administrator<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="peminjam.php"><i class="fa fa-user"></i>Peminjam</a></li>
                            <li><a href="operator.php"><i class="fa fa-user"></i>Operator</a></li>
                            <li class="divider"></li>
                            <li><a href="logout.php"><i class="fa fa-power-off"></i> Log Out</a></li>

                        </ul>
                    </li>
                    <li class="divider-vertical"></li>
                </ul>
            </div>
        </nav>
<div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Form Input
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form action="simpan_inventaris.php" method="post" enctype="multipart/form-data">
                                       <div class="form-group">
                                            <label>ID</label>
                                            <input name="id" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Nama</label>
                                            <input name="nama" class="form-control" placeholder="Masukan Nama Kuliner Anda">
                                        </div>
										<div class="form-group">    
											<label>Harga</label>
                                            <input name="harga" class="form-control" placeholder="Masukan Harga">
                                        </div>
										<div class="form-group">
                                            <label>Lokasi</label>
                                            <input name="lokasi" class="form-control" rows="3" placeholder="Lokasi Kuliner"></input>
										</div>
										 
										</select>
										<button type="submit" class="btn btn-success">Simpan</button>
                                        <button type="reset" class="btn btn-danger">Reset</button>
                                    </form>
                                </div>
							</div>
						</div>
				


            <footer>
                <hr>
										<?php 
										$no++;
												}
												}else{
													echo "0 results";
												} $koneksi->close();
										?>
										</tbody>
									</table>
									<script type="text/javascript" src="assets/js/jquery.min.js"></script>
									<script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
									<script>$(document).ready(function(){
										$('#example').DataTable();
									});
									</script>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
    <!-- /#wrapper -->

    <script type="text/javascript">
        jQuery(function ($) {
            var performance = [12, 43, 34, 22, 12, 33, 4, 17, 22, 34, 54, 67],
                visits = [123, 323, 443, 32],
                traffic = [
                {
                    Source: "Direct", Amount: 323, Change: 53, Percent: 23, Target: 600
                },
                {
                    Source: "Refer", Amount: 345, Change: 34, Percent: 45, Target: 567
                },
                {
                    Source: "Social", Amount: 567, Change: 67, Percent: 23, Target: 456
                },
                {
                    Source: "Search", Amount: 234, Change: 23, Percent: 56, Target: 890
                },
                {
                    Source: "Internal", Amount: 111, Change: 78, Percent: 12, Target: 345
                }];


            $("#shieldui-chart1").shieldChart({
                theme: "dark",

                primaryHeader: {
                    text: "Visitors"
                },
                exportOptions: {
                    image: false,
                    print: false
                },
                dataSeries: [{
                    seriesType: "area",
                    collectionAlias: "Q Data",
                    data: performance
                }]
            });

            $("#shieldui-chart2").shieldChart({
                theme: "dark",
                primaryHeader: {
                    text: "Traffic Per week"
                },
                exportOptions: {
                    image: false,
                    print: false
                },
                dataSeries: [{
                    seriesType: "pie",
                    collectionAlias: "traffic",
                    data: visits
                }]
            });

            $("#shieldui-grid1").shieldGrid({
                dataSource: {
                    data: traffic
                },
                sorting: {
                    multiple: true
                },
                rowHover: false,
                paging: false,
                columns: [
                { field: "Source", width: "170px", title: "Source" },
                { field: "Amount", title: "Amount" },                
                { field: "Percent", title: "Percent", format: "{0} %" },
                { field: "Target", title: "Target" },
                ]
            });            
        });        
    </script>
</body>
</html>
 