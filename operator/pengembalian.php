<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inventaris</title>

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="css/local.css" />

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

    <!-- you need to include the shieldui css and js assets in order for the charts to work -->
    <link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/shieldui-all.min.css" />
    <link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css" />
    <link id="gridcss" rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/dark-bootstrap/all.min.css" />

    <script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="http://www.prepbootstrap.com/Content/js/gridData.js"></script>
</head>
<body>
    <div id="wrapper">
          <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Inventaris</a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
               <ul id="active" class="nav navbar-nav side-nav">
                    <li><a href="index.php"><i class="fa fa-list"></i> Inventarisir</a></li>
                 <li><a href="proses_inventaris.php" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-info"><br></i> Proses Inventaris</a>
					<ul class="dropdown-menu">
                            <li><a href="peminjaman.php"><i class="fa fa-share"></i> Peminjaman</a></li>
                            <li><a href="pengembalian.php"><i class="fa fa-reply"></i> Pengembalian</a></li>
                       </ul>
                    <li><a href="master_data.php" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i> Master Data</a>
					<ul class="dropdown-menu">
                            <li><a href="jenis.php"><i class="fa fa-caret-down"></i> Jenis</a></li>
                            <li><a href="level.php"><i class="fa fa-caret-down"></i> Level</a></li>
							<li><a href="ruang.php"><i class="fa fa-caret-down"></i> Ruang</a></li>
                            <li><a href="petugas.php"><i class="fa fa-caret-down"></i> Petugas</a></li>
                        </ul>
                </ul>
                <ul class="nav navbar-nav navbar-right navbar-user">
           
                    <li class="dropdown user-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>Administrator<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="logout.php"><i class="fa fa-power-off"></i> Log Out</a></li>

                        </ul>
                    </li>
                    
                </ul>
            </div>
        </nav>

				<div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-book"></i> Data Inventaris</h3>
                        </div>
						<div class="panel-body">
							<div class="table-responsive">
							
						<p align="right">
							<a href="input_inventaris.php" ><button type="button" class="btn btn-outline btn-primary fa fa-plus"> Tambah Data</button></a>
					<div class="box-content">
					</p>
								<table id="example" class="table table-striped table-bordered table-hover">
								<thead>
										<tr>
										<th>No</th>
										<th>Nama Barang</th>
										<th>Kondisi</th>
										<th>Keterangan</th>
										<th>Jumlah</th>
										<th>ID Jenis</th>
										<th>Tanggal Register</th>
										<th>ID Ruang</th>
										<th>Kode Inventaris</th>
										<th>ID Petugas</th>
										<th>Action</th>
										</tr>
										</thead>
										<tbody>
										<?php
											$servername="localhost";
											$username="root";
											$password="123";
											$dbname="inventaris_sekolah";
											
											$conn = new mysqli($servername,$username,$password,$dbname);
											
											if($conn->connect_error){
												die("koneksi gagal:" . $conn->connect_error);
											}
											
											$sql = "SELECT * from inventaris a left join jenis p on p.id_jenis=a.id_jenis";
											$hasil = $conn->query($sql);
											$no=1;
											if($hasil->num_rows > 0){
												foreach($hasil as $row){
										?>

								<tr>
									<td><?php echo $no++; ?></td>
									<td><?php echo $row['nama']; ?></td>
									<td><?php echo $row['kondisi']; ?></td>
									<td><?php echo $row['keterangan']; ?></td>
									<td><?php echo $row['jumlah']; ?></td>
									<td><?php echo $row['id_jenis']; ?></td>
									<td><?php echo $row['tanggal_register']; ?></td>
									<td><?php echo $row['id_ruang']; ?></td>
									<td><?php echo $row['kode_inventaris']; ?></td>
									<td><?php echo $row['id_petugas']; ?></td>
									
										<td>
											<a href="edit_inventaris.php?id_inventaris=<?php echo $row['id_inventaris']?>"><button type="button" class="btn btn-primary">Edit</button></a>
											<a href="hapus_inventaris.php?id_inventaris=<?php echo $row['id_inventaris']?>"><button type="button" class="btn btn-danger">Hapus</button></a>
										</td>
								</tr>
										<?php 
										$no++;
												}
												}else{
													echo "";
												} $conn->close();
										?>
										
										</tbody>
									</table>
									<script type="text/javascript" src="assets/js/jquery.min.js"></script>
									<script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
									<script>$(document).ready(function(){
										$('#example').DataTable();
									});
									</script>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
    <!-- /#wrapper -->

    <script type="text/javascript">
        jQuery(function ($) {
            var performance = [12, 43, 34, 22, 12, 33, 4, 17, 22, 34, 54, 67],
                visits = [123, 323, 443, 32],
                traffic = [
                {
                    Source: "Direct", Amount: 323, Change: 53, Percent: 23, Target: 600
                },
                {
                    Source: "Refer", Amount: 345, Change: 34, Percent: 45, Target: 567
                },
                {
                    Source: "Social", Amount: 567, Change: 67, Percent: 23, Target: 456
                },
                {
                    Source: "Search", Amount: 234, Change: 23, Percent: 56, Target: 890
                },
                {
                    Source: "Internal", Amount: 111, Change: 78, Percent: 12, Target: 345
                }];


            $("#shieldui-chart1").shieldChart({
                theme: "dark",

                primaryHeader: {
                    text: "Visitors"
                },
                exportOptions: {
                    image: false,
                    print: false
                },
                dataSeries: [{
                    seriesType: "area",
                    collectionAlias: "Q Data",
                    data: performance
                }]
            });

            $("#shieldui-chart2").shieldChart({
                theme: "dark",
                primaryHeader: {
                    text: "Traffic Per week"
                },
                exportOptions: {
                    image: false,
                    print: false
                },
                dataSeries: [{
                    seriesType: "pie",
                    collectionAlias: "traffic",
                    data: visits
                }]
            });

            $("#shieldui-grid1").shieldGrid({
                dataSource: {
                    data: traffic
                },
                sorting: {
                    multiple: true
                },
                rowHover: false,
                paging: false,
                columns: [
                { field: "Source", width: "170px", title: "Source" },
                { field: "Amount", title: "Amount" },                
                { field: "Percent", title: "Percent", format: "{0} %" },
                { field: "Target", title: "Target" },
                ]
            });            
        });        
    </script>
</body>
</html>
 