<?php
include ('koneksi.php');
?>
<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>Inventaris</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	 <link rel="shortcut icon" href="gambar/ik.jpg">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>

        <script src="lib/jQuery-Knob/js/jquery.knob.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $(".knob").knob();
        });
    </script>


    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/premium.css">
<link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">
</head>
<?php
 function anti ($data)
 {
	   $filter = mysql_real_escape_string(stripcslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
	   return $filter;
 }
 ?>
<body class=" theme-blue">

    <!-- Demo page code -->

    <script type="text/javascript">
        $(function() {
            var match = document.cookie.match(new RegExp('color=([^;]+)'));
            if(match) var color = match[1];
            if(color) {
                $('body').removeClass(function (index, css) {
                    return (css.match (/\btheme-\S+/g) || []).join(' ')
                })
                $('body').addClass('theme-' + color);
            }

            $('[data-popover="true"]').popover({html: true});
            
        });
    </script>
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
            color: #fff;
        }
    </style>

    <script type="text/javascript">
        $(function() {
            var uls = $('.sidebar-nav > ul > *').clone();
            uls.addClass('visible-xs');
            $('#main-menu').append(uls.clone());
        });
    </script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
   
  <!--<![endif]-->

    <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="" href="index2.php"><span class="navbar-brand"><span class="fa fa-cutlery"></span> Inventaris Sekolah </span></a></div>

        <div class="navbar-collapse collapse" style="height: 1px;">
          <ul id="main-menu" class="nav navbar-nav navbar-right">
            <li class="dropdown hidden-xs">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-user padding-right-small" style="position:relative;top: 3px;"></span> Administrator
					
					
                    <i class="fa fa-caret-down"></i>
                </a>

              <ul class="dropdown-menu">
                <li><a tabindex="-1" href="logout.php">Logout</a></li>
              </ul>
            </li>
          </ul>

        </div>
      </div>
    </div>
 
    <div class="sidebar-nav">
    <ul>
   
     <li><a href="index2.php" class="nav-header"><i class="fa fa-fw fa- fa-align-justify"></i> Inventaris</a></li>
	 <li><a href="peminjaman.php" class="nav-header"><i class="fa fa-fw fa- fa-share"></i> Peminjaman </a></li>
	 <li><a href="pengembalian.php" class="nav-header"><i class="fa fa-fw fa- fa-reply"></i> Pengembalian </a></li>
	 <li><a href="masterdata.php" class="nav-header"><i class="fa fa-fw fa- fa-sun-o"></i> Master Data<a></li>
	 
   </ul>
    </div>
    <div class="content">
        <div class="header">
            

            <h1 class="page-title">Tambah Data</h1>
                    <ul class="breadcrumb">
           
        </ul>

        </div>
        <div class="main-content">
            
<div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Form Info
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
								
								<?php 
								$select_jenis=mysql_query("SELECT `id_jenis`, `nama_jenis` from `jenis`");
								$select_ruang=mysql_query("SELECT `id_ruang`, `nama_ruang` from `ruang`");
								$select_petugas=mysql_query("SELECT `id_petugas`, `nama_petugas` from `petugas`");
								?>
                                    <form action="simpan_data.php" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>Nama</label>
                                            <input name="nama" class="form-control" placeholder="Masukan Nama Anda">
                                        </div>
										<div class="form-group">    
											<label>Kondisi</label>
                                            <input name="kondisi" class="form-control" placeholder="Masukan Kondisi Barang">
                                        </div>
										<div class="form-group">
                                            <label>Keterangan</label>
                                            <input name="keterangan" class="form-control" placeholder="Masukan Keterangan">
										</div>
										<div class="form-group">
                                            <label>Jumlah</label>
                                            <input name="jumlah" class="form-control" placeholder="Masukan Jumlah Barang">
										</div>
											<div class="form-group">
                                            <label>Id Jenis</label>
                                            <select class="form-control span8" name="id_jenis" required>
											<?php while ($data_jenis=mysql_fetch_array($select_jenis)){ ?>
											<option value="<?php echo $data_jenis['id_jenis']; ?>"><?php echo $data_jenis ['nama_jenis']; ?>
											</option>
											<?php } ?>
											</select>
											</div>
											
										<div class="form-group">
                                            <label>Id Ruang</label>
                                            <select class="form-control span8" name="id_ruang" required>
											<?php while ($data_ruang=mysql_fetch_array($select_ruang)){ ?>
											<option value="<?php echo $data_ruang['id_ruang']; ?>"><?php echo $data_ruang ['nama_ruang']; ?>
											</option>
											<?php } ?>
											</select>
											</div>
										<div class="form-group">
                                            <label>Kode Inventaris</label>
                                            <input name="kode_inventaris" class="form-control" placeholder="Masukan Kode Inventaris">
										</div>
											
										<div class="form-group">
                                            <label>Id Petugas</label>
                                            <select class="form-control span8" name="id_petugas" required>
											<?php while ($data_petugas=mysql_fetch_array($select_petugas)){ ?>
											<option value="<?php echo $data_petugas['id_petugas']; ?>"><?php echo $data_petugas ['nama_petugas']; ?>
											</option>
											<?php } ?>
											</select>
											</div>
										</select>
										<button type="submit" class="btn btn-success">Simpan</button>
                                        <button type="reset" class="btn btn-danger">Reset</button>
                                    </form>
                                </div>
							</div>
						</div>
				


            <footer>
                <hr>

                <!-- Purchase a site license to remove this link from the footer: http://www.portnine.com/bootstrap-themes -->
                <p class="pull-right">Inventaris</a> 
                <p> © 2018 Inventaris</a></p>
            </footer>
        </div>
    </div>


    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
  
</body></html>
