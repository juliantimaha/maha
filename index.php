<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>Inventaris</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>

        <script src="lib/jQuery-Knob/js/jquery.knob.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $(".knob").knob();
        });
    </script>


    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/premium.css">
	

	 <link href="aa/../../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="aa/../../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="aa/../../css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="aa/../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="aa/../../css/AdminLTE.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/jquery.datatables/dataTables.css">
</head>
<body class=" theme-blue">

    <!-- Demo page code -->

    <script type="text/javascript">
        $(function() {
            var match = document.cookie.match(new RegExp('color=([^;]+)'));
            if(match) var color = match[1];
            if(color) {
                $('body').removeClass(function (index, css) {
                    return (css.match (/\btheme-\S+/g) || []).join(' ')
                })
                $('body').addClass('theme-' + color);
            }

            $('[data-popover="true"]').popover({html: true});
            
        });
    </script>
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
            color: #fff;
        }
    </style>

    <script type="text/javascript">
        $(function() {
            var uls = $('.sidebar-nav > ul > *').clone();
            uls.addClass('visible-xs');
            $('#main-menu').append(uls.clone());
        });
    </script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
   
  <!--<![endif]-->

    <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="" href="index.html"><span class="navbar-brand"><span class="fa fa-paper-plane"></span> Inventaris Sekolah </span></a></div>

        <div class="navbar-collapse collapse" style="height: 1px;">
          <ul id="main-menu" class="nav navbar-nav navbar-right">
            <li class="dropdown hidden-xs">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-user padding-right-small" style="position:relative;top: 3px;"></span> Administrator
                    <i class="fa fa-caret-down"></i>
                </a>

              <ul class="dropdown-menu">
                <li><a href="./">My Account</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Admin Panel</li>
                <li><a href="./">Users</a></li>
                <li><a href="./">Security</a></li>
                <li><a tabindex="-1" href="./">Payments</a></li>
                <li class="divider"></li>
                <li><a tabindex="-1" href="logout.php">Logout</a></li>
              </ul>
            </li>
          </ul>

        </div>
      </div>
    </div>
    

    <div class="sidebar-nav">
    <ul>
		<li><a href="index2.php" class="nav-header"><i class="fa fa-fw fa- fa-align-justify"></i> Inventaris</a></li>
	    <li><a href="peminjaman.php" class="nav-header"><i class="fa fa-fw fa- fa-share"></i> Peminjaman </a></li>
	    <li><a href="pengembalian.php" class="nav-header"><i class="fa fa-fw fa- fa-reply"></i> Pengembalian </a></li>
	    <li><a href="#" data-target=".legal-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-gears "></i> Master Data<i class="fa fa-collapse"></i></a></li>
        <li><ul class="legal-menu nav nav-list collapse">
        <li ><a href="jenis.php"><span class="fa fa-caret-right"></span> Jenis </a></li>
        <li ><a href="ruang.php"><span class="fa fa-caret-right"></span> Ruang </a></li>
        <li ><a href="pegawai.php"><span class="fa fa-caret-right"></span> Pegawai </a></li>
        <li ><a href="petugas.php"><span class="fa fa-caret-right"></span> Petugas </a></li>
             
    </ul>
	<li><a href="laporan.php" class="nav-header"><i class="fa fa-pencil-square-o"></i> Laporan <a></li>
    <li><a href="logout.php" class="nav-header"><i class="fa  fa-spinner"></i> Logout <a></li>
    </div>

    <div class="content">
        <div class="header">
            <h1 class="page-title"> Data Inventaris </h1>
                    
 </div><div class="panel panel-default">
 <div class="col-lg-12">
                               
                                <div class="panel-body">
								<div class="table-responsive">
                                    <table id="example" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
											<th>ID Inventaris</th>
                                          <th>Nama</th>
                                          <th>kondisi</th>
                                          <th>keterangan</th>
                                          <th>jumlah</th>
                                          <th>jenis</th>
                                          <th>tanggal register</th>
                                          <th>ruang</th>
                                          <th>kode inventaris</th>
                                          <th>aksi</th>
										  </tr>
										  </thead>
                                       <tbody>
									   <?php
									   include "koneksi.php";
									   $no=1;
									   $select=mysql_query("select * from inventaris");
									   while($data=mysql_fetch_array($select))
									   {
										   
										  ?>
											
												<td><?php echo $data['id_inventaris']; ?></td>
												<td><?php echo $data['nama']; ?></td>
												<td><?php echo $data['kondisi']; ?></td>
												<td><?php echo $data['keterangan']; ?></td>
												<td><?php echo $data['jumlah']; ?></td>
												<td><?php echo $data['id_jenis']; ?></td>
												<td><?php echo $data['tanggal_register']; ?></td>
												<td><?php echo $data['id_ruang']; ?></td>
												<td><?php echo $data['kode_inventaris']; ?></td>
												
												<td>
												<a href="hapus_jenis.php?id_jenis=<?php echo $row['id_jenis']; ?>"><button type="button" class="btn btn-danger"> Hapus </button>
												<a href="edit_jenis.php?id_jenis=<?php echo $row['id_jenis']; ?>"><button type="button" class="btn btn-info"> Edit </button>
										</td>
											</tr>
											<?php 
									   }
									   ?>
									   </tbody>
										</table>
										
                                    </table>
									<script type="text/javascript" src="assets/js/jquery.js"></script>
										<script type="text/javascript" src="assets/js/jquery.min.js"></script>
										<script type="text/javascript" src="assets/js/jquery.dataTables.js"></script>
										<script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
										
										<script>
										$(document).ready(function() {
											$('#example').DataTable();
										});
										</script>
                                       
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->            </div>
                    </div>

            <footer>
                <hr>

                <!-- Purchase a site license to remove this link from the footer: http://www.portnine.com/bootstrap-themes -->
                <p class="pull-right">A <a href="http://www.portnine.com/bootstrap-themes" target="_blank">Free Bootstrap Theme</a> by <a href="http://www.portnine.com" target="_blank">Portnine</a></p>
                <p>© 2014 <a href="http://www.portnine.com" target="_blank">Portnine</a></p>
            </footer>
        </div>
    </div>
	
	 <!-- jQuery 2.0.2 -->
        <script src="aa/../../js/jquery-1.11.2.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="aa/../../js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="aa/../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="aa/../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="aa/../../js/AdminLTE/app.js" type="text/javascript"></script>

	
<script type="aa/text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>


    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
	
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='bower_components/moment/min/moment.min.js'></script>
<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='js/jquery.dataTables.min.js'></script>

<!-- select or dropdown enhancer -->

<script src="js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="js/charisma.js"></script>


    
  
</body></html>
