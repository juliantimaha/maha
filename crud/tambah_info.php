<?php
include ('cek.php');
?>
<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>Kuliner Bogor</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	 <link rel="shortcut icon" href="gambar/ik.jpg">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>

        <script src="lib/jQuery-Knob/js/jquery.knob.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $(".knob").knob();
        });
    </script>


    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/premium.css">
<link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">
</head>
<?php
 function anti ($data)
 {
	   $filter = mysql_real_escape_string(stripcslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
	   return $filter;
 }
 ?>
<body class=" theme-blue">

    <!-- Demo page code -->

    <script type="text/javascript">
        $(function() {
            var match = document.cookie.match(new RegExp('color=([^;]+)'));
            if(match) var color = match[1];
            if(color) {
                $('body').removeClass(function (index, css) {
                    return (css.match (/\btheme-\S+/g) || []).join(' ')
                })
                $('body').addClass('theme-' + color);
            }

            $('[data-popover="true"]').popover({html: true});
            
        });
    </script>
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
            color: #fff;
        }
    </style>

    <script type="text/javascript">
        $(function() {
            var uls = $('.sidebar-nav > ul > *').clone();
            uls.addClass('visible-xs');
            $('#main-menu').append(uls.clone());
        });
    </script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
   
  <!--<![endif]-->

    <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="" href="index.php"><span class="navbar-brand"><span class="fa fa-cutlery"></span> Kuliner Bogor</span></a></div>

        <div class="navbar-collapse collapse" style="height: 1px;">
          <ul id="main-menu" class="nav navbar-nav navbar-right">
            <li class="dropdown hidden-xs">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-user padding-right-small" style="position:relative;top: 3px;"></span> Kuliner Bogor
                    <i class="fa fa-caret-down"></i>
                </a>

              <ul class="dropdown-menu">
                <li><a tabindex="-1" href="logout.php">Logout</a></li>
              </ul>
            </li>
          </ul>

        </div>
      </div>
    </div>
 <div class="sidebar-nav">
	<ul>
	<li><a href="#" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-home"></i>&nbsp Index<i class="fa fa-collapse"></i></a></li>
	<li><ul class="dashboard-menu nav nav-list collapse">
    <li><a href="index_view.php"><span class="fa fa-caret-right"></span> View</a></li></ul></li>
    <li><a href="#" data-target=".premium-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-picture-o"></i>&nbsp Gallery<i class="fa fa-collapse"></i></a></li>
    <li><ul class="premium-menu nav nav-list collapse">
    <li><a href="view.php"><span class="fa fa-caret-right"></span> View</a></li>
    <li><a href="upload.php"><span class="fa fa-caret-right"></span> Upload</a></li>
    </ul></li>
	<li><a href="#" data-target=".accounts-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-list"></i>&nbsp Jenis Produk<i class="fa fa-collapse"></i></a></li>
    <li><ul class="accounts-menu nav nav-list collapse">
    <li ><a href="makanan.php"><span class="fa fa-cutlery"></span> Makanan</a></li>
    <li ><a href="minuman.php"><span class="fa fa-coffee"></span> Minuman</a></li>
    </ul></li>
	<li><a href="tambah_produk.php" data-target=".legal-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-plus"></i>&nbsp Tambah Produk</i></a></li>
    <li><a href="info.php" data-target=".legal-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-info"></i>&nbsp Info</i></a></li>
	<li><a href="contact.php" data-target=".legal-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-phone"></i>&nbsp Contact</i></a></li>
	<li><a href="saran_admin.php" class="nav-header"><i class="fa fa-fw fa-comment"></i>&nbsp Saran & Kritik</a></li>
	<li><a href="deskripsi.php" class="nav-header"><i class="fa fa-fw fa-file-o"></i>&nbsp Deskripsi</a></li></ul>
   </div>
    <div class="content">
        <div class="header">
            

            <h1 class="page-title">Kuliner Bogor</h1>
                    <ul class="breadcrumb">
           
        </ul>

        </div>
        <div class="main-content">
            



<div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Form Info
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form action="simpan_info.php" method="post" enctype="multipart/form-data">
                                       <div class="form-group">
                                            <label>ID</label>
                                            <input name="id" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Nama</label>
                                            <input name="nama" class="form-control" placeholder="Masukan Nama Kuliner Anda">
                                        </div>
										<div class="form-group">    
											<label>Harga</label>
                                            <input name="harga" class="form-control" placeholder="Masukan Harga">
                                        </div>
										<div class="form-group">
                                            <label>Lokasi</label>
                                            <input name="lokasi" class="form-control" rows="3" placeholder="Lokasi Kuliner"></input>
										</div>
										 
										</select>
										<button type="submit" class="btn btn-success">Simpan</button>
                                        <button type="reset" class="btn btn-danger">Reset</button>
                                    </form>
                                </div>
							</div>
						</div>
				


            <footer>
                <hr>

                <!-- Purchase a site license to remove this link from the footer: http://www.portnine.com/bootstrap-themes -->
                <p class="pull-right">Kuliner Bogor</a> 
                <p> © 2018 Kuliner Bogor</a></p>
            </footer>
        </div>
    </div>


    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
  
</body></html>
